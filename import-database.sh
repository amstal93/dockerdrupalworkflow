#!/bin/bash
# debug
# set -o xtrace
source .env
set -eux
echo "Volcando la BBDD"
export FILENAME=drupal-export.sql
docker-compose exec -e FILENAME=$FILENAME mariadb /bin/bash -c 'mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < "/tmp/${FILENAME}"'
