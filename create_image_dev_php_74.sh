#!/bin/bash
source .env
echo "Creating PHP-FPM Image ${DOCKER_IMAGE}"
docker build -t $DOCKER_IMAGE_BASE:latest .
docker build -t $DOCKER_IMAGE_BASE:$DOCKER_IMAGE_LAST_VERSION .
#    --no-cache
echo "Login in"
docker login
echo "Uploading the PHP-FPM image to the repository"
docker push $DOCKER_IMAGE_BASE:latest
docker push $DOCKER_IMAGE_BASE:$DOCKER_IMAGE_LAST_VERSION
