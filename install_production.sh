#!/bin/bash
set -eux
docker-compose -f docker-compose-prod.yaml exec \
  solr-prod \
  solr create_core -c drupal -d /opt/solr/server/solr/drupal

./import-database-prod.sh