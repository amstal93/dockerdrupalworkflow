#!/bin/bash
source .env
set -eux
#import all configs
sudo cp -r ./drupal_etc/drupal_sync/* ./volumes-$ENV_STAGE/app-tmp/import
docker-compose exec -u www-data \
  app drush  -y \
  config-import --partial --source /tmp/import