#!/bin/bash
set -eux
source .env
echo "Downloading composer dependencies"
sudo composer install -d ./drupal
echo "Creating PHP-FPM Image :${DOCKER_IMAGE_PROD_NAME}:${DOCKER_IMAGE_PROD_VERSION}"
docker build -t ${DOCKER_IMAGE_PROD_NAME}:${DOCKER_IMAGE_PROD_VERSION} -f ./Dockerfile.prod .
docker build -t ${DOCKER_IMAGE_PROD_NAME}:latest -f ./Dockerfile.prod .

echo "Login in"
docker login
echo "Uploading the image to the repository"
docker push ${DOCKER_IMAGE_PROD_NAME}:${DOCKER_IMAGE_PROD_VERSION}
docker push ${DOCKER_IMAGE_PROD_NAME}:latest

