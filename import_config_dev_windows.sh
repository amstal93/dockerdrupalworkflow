#!/bin/bash
source .env
set -eux
chmod 777 ./volumes-dev/app-tmp
mkdir -p ./volumes-dev/app-tmp/import
cp -r ./drupal_etc/drupal_sync/* ./volumes-dev/app-tmp/import
docker-compose exec -u www-data   app drush  -y config-import --partial --source //tmp/import