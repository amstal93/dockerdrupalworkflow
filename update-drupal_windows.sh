#!/bin/bash
#!/bin/bash
set -eux

./backup-database_windows.sh

docker-compose exec -u www-data app  drush state-set system.maintenance_mode 1

docker-compose exec -u www-data app drush cache-rebuild

docker-compose exec app  composer update drupal/bootstrap_barrio --with-dependencies -d //var/www

docker-compose exec -u www-data app  drush -y updatedb

docker-compose exec -u www-data app drush -y state-set system.maintenance_mode 0

docker-compose exec -u www-data app  drush -y cache-rebuild

docker-compose exec -u www-data app drush  -y locale-update

docker-compose exec -u www-data app drush  -y cron

./backup-database_windows.sh