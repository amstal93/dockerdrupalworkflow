#!/bin/bash
source .env
set -eux
# Backup database previous to update
./backup-database.sh
# Maintenance mode on
docker-compose exec -u www-data \
  app  \
  drush state-set system.maintenance_mode 1
# Cache Rebuild
docker-compose exec -u www-data \
  app  \
  drush cache-rebuild
# Update modules
#docker-compose exec \
#  app  \
#  composer update drupal/inline_entity_form --with-dependencies -d /var/www
docker-compose exec \
  app  \
 composer update drupal/avd_varnish  --with-all-dependencies -d /var/www
docker-compose exec \
  app  \
  composer update drupal/entity_api --with-dependencies -d /var/www
# Apply database updates
#docker-compose exec -u www-data \
#  app  \
#  drush -y \
#  updatedb

#drush pm-update drupal
#drush pm-update --security-only
# Update drupal/core-recommended
docker-compose exec \
  app  \
  composer update drupal/core -W -d /var/www
# Apply database updates for core
docker-compose exec -u www-data \
  app  \
  drush -y \
  updatedb
# Maintenance OFF
docker-compose exec -u www-data \
  app  \
  drush -y \
  state-set system.maintenance_mode 0
# Cache Rebuild
docker-compose exec -u www-data \
  app  \
  drush -y \
  cache-rebuild
# Update Locales
docker-compose exec -u www-data \
  app drush  -y \
  locale-update
# for production
# composer install --no-dev -d /var/www
# Sync config files
#docker-compose exec -u www-data \
#  app drush  -y \
#  config-import --partial --source /tmp/import
# Drush Cron
docker-compose exec -u www-data \
  app drush  -y \
  cron
# Backup database after to update
./backup-database.sh
