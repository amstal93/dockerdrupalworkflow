#!/bin/bash
source .env
set -eux
docker-compose down
sudo rm -rf ./volumes-$ENV_STAGE

./create_dev_volumes.sh
sudo cp ./drupal_etc/default.settings.php ./drupal_etc/settings-$ENV_STAGE.php
docker-compose up -d
sleep 10
sudo docker cp ./sql_dump/drupal-export-latest.sql mariadb-$ENV_STAGE:/tmp/drupal-export.sql
#sudo chmod -R 777 ./volumes-$ENV_STAGE/mariadb-tmp
./import-database.sh
# update permissions to $UID and $GID user and group
docker-compose exec -u root app chown -R $DUID:$DGID /var/www
sudo chown -R $DUID:$DGID ./volumes-$ENV_STAGE/app-tmp
sudo chmod -R 777 ./volumes-$ENV_STAGE/app-tmp
docker-compose exec -u root app composer install -d /var/www
docker-compose exec solr solr create_core -c drupal -d /opt/solr/server/solr/drupal
docker-compose exec app drush -y updatedb
docker-compose exec -u www-data app drush -y cron
docker-compose exec -u www-data app drush -y cr

