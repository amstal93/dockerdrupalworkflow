#!/bin/bash
source .env
set -eux

#import all configs
docker-compose exec -u www-data \
  app drush  -y \
  config-export --destination /tmp/import
sudo cp ./volumes-$ENV_STAGE/app-tmp/import/* ./drupal_etc/drupal_sync_all
for d in $(find ./drupal_etc/drupal_sync_all -maxdepth 2 -type d); do for file in $d/*; do if [ -e $file ]; then echo $file; fi; done; done; 