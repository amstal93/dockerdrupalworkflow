#!/bin/bash
source .env
echo "Creating PHP-FPM 8.0 Image ${DOCKER_IMAGE_80}"
docker build -t $DOCKER_IMAGE_BASE_80:latest  -f Dockerfile.dev.php80 .
docker build -t $DOCKER_IMAGE_BASE_80:$DOCKER_IMAGE_LAST_VERSION_80 -f Dockerfile.dev.php80 .
#    --no-cache
echo "Login in"
docker login
echo "Uploading the PHP-FPM 8.0 image to the repository"
docker push $DOCKER_IMAGE_BASE_80:latest
docker push $DOCKER_IMAGE_BASE_80:$DOCKER_IMAGE_LAST_VERSION_80
