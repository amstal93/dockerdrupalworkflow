@d8 @api
Feature: Article Taxonomy
  Check Article Taxonomy
  
  Scenario Outline: Test that taxonomy vocabulary is created

    Given I am not logged in
    When I visit "/user"

    Then I should see "Username"
    When I enter "admin" for "Username"
    And I enter "admin" for "Password"
    And I press the "Log in" button

    Then I should see the link "View"
    And I should see the link "Edit"

    When I am on "/admin/structure/taxonomy"
    Then I should see the text <taxonomy>

    Examples:
      | taxonomy |
      |  "Etiquetas"   |
      
   Scenario Outline: Test that taxonomy items are created

    Given I am not logged in
    When I visit "/user"

    Then I should see "Username"
    When I enter "admin" for "Username"
    And I enter "admin" for "Password"
    And I press the "Log in" button

    Then I should see the link "View"
    And I should see the link "Edit"

    When I am on "/admin/structure/taxonomy/manage/tags/overview"
    Then I should see the text <items>

    Examples:
      | items |
      |  "No terms available"   |