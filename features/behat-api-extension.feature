Feature: Drupal RestFul Api Testing with Behat


  Scenario: Get access to JSONAPI
    Given I am authenticating as "admin" with password "admin"
    When I request "/jsonapi"
    Then the response code is 200
  Scenario: Get access to /node/article
    Given I am authenticating as "admin" with password "admin"
    When I request "/jsonapi/node/article"
    Then the response code is 200

  #Scenario: Post new article
  #  Given I am authenticating as "admin" with password "admin"
  #  And the "Content-Type" request header is "application/vnd.api+json"
  #  And the "Accept" request header is "application/vnd.api+json"
  #  And the "Autentication" request header is "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQ1YTVkZWM0NGNkMjUxM2RiYzM0YmNkODRlN2EzMDJjN2FjMWNlY2RkY2QwMmFlYjAxYjhjNmFkZDI0NjQ3NGJhNzEyZGIzYTg1MTlkMjI3In0.eyJhdWQiOiJiNzE0YjkzYy1lNjEwLTRjMDItODNiMC04OTg1Zjc5MDI3MmQiLCJqdGkiOiJkNWE1ZGVjNDRjZDI1MTNkYmMzNGJjZDg0ZTdhMzAyYzdhYzFjZWNkZGNkMDJhZWIwMWI4YzZhZGQyNDY0NzRiYTcxMmRiM2E4NTE5ZDIyNyIsImlhdCI6MTYwNTI2MjUyMCwibmJmIjoxNjA1MjYyNTIwLCJleHAiOjE2MDUyNjU1MTksInN1YiI6IjEiLCJzY29wZXMiOlsiYXV0aGVudGljYXRlZCIsImNvbnN1bWVyIl19.TSfVEgOxu2CpCQZCqiVNc87730ovOEh3II1jejhSR9jhSib416IpHOQtHmXXucCbr9OmFiqPgSRtnvYw04URXuQe2qqv27bFtUdFaSydQvTodtlQ_XG4AK1V-MsglnjLGeYNyuLZqzeCwUtVIuKD3f2YYcJPvYndqTzZ2Bi1d15pKY8km_eOYBMzrfo0egCo3u6fZI-XKIF3T0WMaSuW1f824O8t00kHGoxS9qgOj57qLnZQtBZ98Iy0hFJcl1-mHGFSmBwWks83uHBmLQ7Ro_HLaePa__9WGGj1rpZatcC0FP3P-ueOBauyn1iLsgD80698AE2cHSumR4JTh_aUrw"
  #  And the request body is:
  #  """
  #  {
  #    "data": {
  #      "type": "node--article",
  #      "attributes": {
  #       "status": true,
  #        "title": "title1",
  #        "default_langcode": true,
  #        "body": null
  #      }
  #    }
  # }
  #  """
  #  When I request "/jsonapi/node/page" using HTTP "POST"
  #  Then the response code is 200



